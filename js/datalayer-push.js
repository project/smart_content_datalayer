/**
 * @file
 * Drupal behavior to push smart content winner details to the dataLayer.
 */

(function (Drupal) {
  window.addEventListener('smart_content_decision', function (event) {
    // Format data to push into the data layer.
    var winnerDetails = event.detail.settings.dataLayer[event.detail.winner];
    var data = {
      label: winnerDetails.label,
      default: event.detail.default,
      reactions: winnerDetails.reactions
    };
    var scString = JSON.stringify(data);

    // Push or append data to the dataLayer.
    var index = window.dataLayer.findIndex(function (object) {
      return object.hasOwnProperty('smart_content');
    });
    if (index > -1) {
      // Smart content data already exists in the dataLayer.
      var currentData = window.dataLayer[index].smart_content.split(';');
      currentData.push(scString);
      currentData.sort();
      var newString = currentData.join(';');
      window.dataLayer.push({
        'smart_content': newString
      });
    }
    else {
      window.dataLayer.push({
        'smart_content': scString
      });
    }

  });

})(Drupal);
