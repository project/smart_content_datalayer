INTRODUCTION
------------

This module provides data layer integration with Smart Content. It adds rendered
smart content


REQUIREMENTS
------------

This module requires the following modules:

 * Smart Content (https://www.drupal.org/project/smart_content)


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

No configuration is needed for the module in Drupal.

The module pushes Smart Content segment
information to the dataLayer in the following format:

{label: Segment Label, default: false, reactions: [Reaction 1, Reaction 2]}

If there are multiple Smart Content segments on a single page, the data is
pushed to the dataLayer as a single string of objects, separated by a semi-
colon. For example:

{label: Label 1, default: true, reactions: [Reaction A, Reaction B]};
{label: Label 2, default: false, reactions: [Reaction X, Reaction Y]}

This value can be retrieved in Google Tag Manager through a data layer variable,
named smart_content.


MAINTAINERS
-----------

Current maintainers:
 * Michael Lander (michaellander) - https://www.drupal.org/u/michaellander
 * Gurwinder Antal (gantal) - https://www.drupal.org/u/gantal
